using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class Program
    {
        class qs
        {
            private int n;
            List<int> arr = new List<int>();

            public void addNum(int a)
            {
                arr.Add(a);
            }

            public void print(int size)
            {
                for (int i = 0; i < size; i++)
                {
                    Console.Write(arr[i] + " ");
                }
            }

            int part(int bottom, int top) //vzame zadnji element za pivota in ga postavi na pravilno mesto, elementi manjsi od pivota so levo od njega, elementi vecji pa desno
            {
                int piv = arr[bottom];
                int left = bottom;
                int right = top;
                while (left < right)
                {
                    while (arr[left] <= piv && left < top)
                    {
                        left++;
                    }

                    while (arr[right] >= piv && right > bottom)
                    {
                        right--;
                    }

                    if (left < right)
                    {
                        int temp = arr[left];
                        arr[left] = arr[right];
                        arr[right] = temp;
                    }
                }
                int temp1 = arr[bottom];
                arr[bottom] = arr[right];
                arr[right] = temp1;
                return right;
            }

            public void qs_alg(int bottom, int top) //funkcija poskrbi za pravilne rekurzivne klice
            {
                if (bottom < top)
                {
                    int j = part(bottom, top);
                    qs_alg(bottom, j - 1);
                    qs_alg(j + 1, top);
                }
            }
        }

        class dfs
        {
            private int numNodes; //stevilo vozlisc
            private List<int>[] a; //polje listov (rabimo za sosede)
            private bool[] visit;

            public dfs(int n) // konsturktor (public, ker ga klicem v main)
            {
                numNodes = n;
                a = new List<int>[n];
                for (int i = 0; i < n; i++)
                {
                    a[i] = new List<int>();
                }
            }

            public void newLink(int node1, int node2)
            {
                a[node1].Add(node2);
            }

            void init() //inicializira vrednosti povezav na 0
            {
                visit = new bool[numNodes];
                for (int i = 0; i < numNodes; i++)
                {
                    visit[i] = false;
                }
            }

            void dfs_recursive(int n)
            {
                visit[n] = true;
                Console.Write(n + " ");
                List<int> adjList = a[n];
                for (int i = 0; i < adjList.Capacity; i++) //capacity pove koliko elementov lahko zadzri list, brez da se poveca
                {
                    if (visit[i] == false)
                    {
                        dfs_recursive(i);
                    }
                }
            }

            public void dfs_alg(int n)
            {
                init();
                dfs_recursive(n);
            }
        }

        struct node
        {
            public int predecessor;
            public int length;
            public int status;
            public int index;
            public int name;
        }

        List<node> nodes = new List<node>();

        static void fillNodes(ref List<node> V, ref int n)
        {
            for (int i = 0; i < n; i++)
            {
                V.Add(new node());
            }
        }

        static void Menu()
        {
            Console.WriteLine("1. Depth First Search");
            Console.WriteLine("2. Quick Sort");
            Console.WriteLine("3. Sieve of Eratosthenes");
            Console.WriteLine("4. End");
        }

        class soe
        {
            private int n;
            private bool[] primes;

            public soe(int num)
            {
                n = num;
                primes = new bool[num + 1];
            }
            public void soe_alg()
            {
                for (int i = 0; i < n + 1; i++) // vsa stevila za zacetek oznacimo, da so prastevila
                {
                    if (i < 2)
                    {
                        primes[i] = false;
                    }
                    else
                    {
                        primes[i] = true;
                    }
                }
                for (int i = 2; i * i < n + 1; i++) //zacnemo pri stevilu 2 in ce je stevilo prastevilo odstranimo njegove veckratnike
                {
                    if (primes[i] == true)
                    {
                        for (int j = i * i; j < n + 1; j += i)
                        {
                            primes[j] = false;
                        }
                    }
                }

                int num = 0;
                for (int i = 0; i < n + 1; i++)
                {
                    if (primes[i] == true)
                    {
                        num++;
                    }
                }

                if (num > 0)
                {
                    for (int i = 0; i < n + 1; i++)
                    {
                        if (primes[i] == true)
                        {
                            Console.Write(i + " ");
                        }
                    }
                }
                else
                {
                    Console.WriteLine("none");
                }
            }
        }

        static void Main(string[] args)
        {
            bool run = true;
            do
            {
                Menu();
                int select = Convert.ToInt32(Console.ReadLine());
                switch (select)
                {
                    case 1:
                        {
                            Console.WriteLine("Vnesite stevilo elementov v grafu: ");
                            int size = Convert.ToInt32(Console.ReadLine());
                            dfs graph = new dfs(size);
                            bool running = true;
                            Console.WriteLine("Vnesite stevilo povezav v grafu: ");
                            int links = Convert.ToInt32(Console.ReadLine());
                            for (int i = 0; i < links; i++)
                            {
                                Console.WriteLine("Vnesite povezavo (najprej prvo vozlisce, nato se drugo) : ");
                                int first = Convert.ToInt32(Console.ReadLine());
                                int second = Convert.ToInt32(Console.ReadLine());
                                graph.newLink(first, second);
                            }
                            graph.dfs_alg(2);
                            Console.WriteLine(Environment.NewLine);
                            break;
                        }
                    case 2:
                        {
                            Console.WriteLine("Input the size of array: ");
                            int n = Convert.ToInt32(Console.ReadLine());
                            qs quicksort = new qs();
                            for (int i = 0; i < n; i++)
                            {
                                Console.WriteLine("Input a number: ");
                                quicksort.addNum(Convert.ToInt32(Console.ReadLine()));
                            }

                            Console.WriteLine("Array before sorting: ");
                            quicksort.print(n);
                            Console.Write(Environment.NewLine);
                            Console.WriteLine("Array after sorting: ");
                            quicksort.qs_alg(0, n - 1);
                            quicksort.print(n);
                            Console.Write(Environment.NewLine);
                            break;
                        }
                    case 3:
                        {
                            Console.WriteLine("Input a positive number: ");
                            int n = Convert.ToInt32(Console.ReadLine());
                            if (n < 0)
                            {
                                Console.WriteLine("Your number is not positive.");
                            }
                            else
                            {
                                Console.WriteLine("Prime numbers smaller or equal to your number are: ");
                                soe s = new soe(n);
                                s.soe_alg();
                            }

                            Console.WriteLine(Environment.NewLine);
                            break;
                        }
                    case 4:
                        run = false;
                        break;
                    default:
                        Console.WriteLine("Wrong choice.");
                        break;
                }
            } while (run);
        }
    }
}